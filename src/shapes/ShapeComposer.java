package shapes;
import java.awt.Point;

public interface ShapeComposer {
	
	public void create(Point position);
	
	public void expand(Point position);
	
}
