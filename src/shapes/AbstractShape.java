package shapes;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;

/**
 * base class AbstractShape
 *
 */
public abstract class AbstractShape implements ShapeComposer {
	protected Point startPoint;
	protected Point endPoint;
	protected Color color;

	public AbstractShape() {
		// default color is red, try your combinations
		color = Color.red;
	}
	
	@Override
	public void create(Point position) {
		setStart(position);
	}

	public void setStart(Point pt) {
		startPoint = pt;
	}

	public void setEnd(Point pt) {
		endPoint = pt;
	}

	public Point getStart() {
		return startPoint;
	}

	public Point getEnd() {
		return new Point(0, 0);
	}

	public abstract void Draw(Graphics2D g);

}

// Base class cShape ends
