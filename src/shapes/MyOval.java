package shapes;
import java.awt.Color;
import java.awt.Graphics2D;

/**
 *  Class MyOval for drawing ovals is derived
 *  from our 'base class' AbstratShape
 */
public class MyOval extends AbstractComplexShape {

	// Drawing routine
	public void Draw(Graphics2D g) {
		g.setColor(Color.green.darker()); // Set default color
		g.drawOval(startPoint.x, startPoint.y, width, height);
	}

}

// Class cOval ends
