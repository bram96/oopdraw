package shapes;

import java.awt.Graphics2D;
import java.awt.Point;

/**
 *  Class MyLine for drawing lines is
 *  derived from our 'base' class AbstractShape
 */
public class MyLine extends AbstractShape {

	// Drawing routine
	public void Draw(Graphics2D g) {
		g.setColor(color); // Set default color -you may ignore colors
		g.drawLine(startPoint.x, startPoint.y, endPoint.x, endPoint.y);
	}
	
	@Override
	public void expand(Point position) {
		setEnd(position);
		
	}
}

// Class cLine ends
