package shapes;

import java.awt.Point;

public abstract class AbstractComplexShape extends AbstractShape {
	
	protected int width;
	protected int height;

	public void setWidth(int w) {
		width = w;
	}

	public void setHeight(int h) {
		height = h;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	@Override
	public void expand(Point position) {
		Point drawto = new Point(Math.max(position.x, startPoint.x), Math.max(position.y, startPoint.y));
		Point newstart = new Point(Math.min(position.x, startPoint.x), Math.min(position.y, startPoint.y));
		int nwidth1 = Math.abs((drawto.x - newstart.x));
		int nheight1 = Math.abs((drawto.y - newstart.y));
		this.setWidth(nwidth1);
		this.setHeight(nheight1);
		this.setStart(newstart);
		
	}
}
