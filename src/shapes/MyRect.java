package shapes;
import java.awt.Color;
import java.awt.Graphics2D;


// Class cRect for drawing Rects is derived
// from our 'base class' cShape

public class MyRect extends AbstractComplexShape {

	// Drawing routine
	public void Draw(Graphics2D g) {
		g.setColor(Color.blue.brighter()); // Set default color
		g.drawRect(startPoint.x, startPoint.y, width, height);
	}

}

// Class cRect ends
